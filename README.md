[![Flatris](flatris.png)](https://flatris.space/)



## Install and run
```
git clone https://gitlab.com/ismoil_nosr/flatris.git
cd flatris/

yarn install
yarn test
yarn build
yarn start
```

## Install to Docker container
```
git clone https://gitlab.com/ismoil_nosr/flatris.git
cd flatris/

docker-compose up -d
```

## Install to Kubernetes cluster

```
kubectl create deployment flatris --image=registry.gitlab.com/ismoil_nosr/flatris:latest 
kubectl expose deployment flatris --port=3000 --target-port=3000 --type=LoadBalancer
```

.

### Project runs in Docker container - [DEMO](http://s303067.savps.ru:3000/)
### Project runs in Kubernetes cluster - [DEMO](http://flatris-ismoil-nosr.cloud.okteto.net/)

.

## Useful links
> ### Get free trial VPS to install and run project - https://www.smartape.eu/ssd-vps
> ### Get free trial to try Kubernetes - https://okteto.com/
> need to mention: those providers doesn't require credit card!
